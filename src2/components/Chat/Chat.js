import React, { useState, useEffect } from 'react';
import queryString from 'query-string';
import io from 'socket.io-client';

import './Chat.css';
import InfoBar from '../InfoBar/InfoBar';
import Input from '../Input/Input';
import Messages from '../Messages/Messages';
import Users from '../Users/Users';
import ScrollToBottom from 'react-scroll-to-bottom';
// import { Redirect } from 'react-router-dom';

let socket;

// const redirect = () => {
//     console.log('redirect');
//     return <Redirect from="/chat" to="/" />;
// };

const ENDPOINT = 'localhost:5000';

const Chat = ({ location }) => {
    const [name, setName] = useState('');
    const [room, setRoom] = useState('');
    const [message, setMessage] = useState('');
    const [messages, setMessages] = useState([]);

    useEffect(() => {
        const { name, room } = queryString.parse(location.search);

        socket = io(ENDPOINT);

        setName(name);
        setRoom(room);

        socket.emit('join', { name, room }, (err) => {
            console.log(err);
            if (err) alert(`${err.err}. Press x to back to Join Page`);

            // redirect();
        });

        return () => {
            socket.emit('disconnect');
            socket.off();
        };
    }, [ENDPOINT, location.search]);

    useEffect(() => {
        socket.on('message', (message) => setMessages([...messages, message]));
    }, [messages]);

    const sendMessage = (event) => {
        event.preventDefault();

        if (message) {
            socket.emit('sendMessage', message, () => setMessage(''));
        }
    };

    console.log('message, messages >>> ', message, messages);

    return (
        <div className="outerContainer">
            <div className="Container">
                <InfoBar room={room} />
                <div className="subContainer">
                    <Users room={room} />
                    <Messages messages={messages} name={name} />
                </div>
                <Input
                    message={message}
                    setMessage={setMessage}
                    sendMessage={sendMessage}
                />
            </div>
        </div>
    );
};

export default Chat;
