import React from 'react';
import ScrollToBottom from 'react-scroll-to-bottom';

import './Users.css';

const Users = ({ room }) => {
    return (
        <ScrollToBottom className="users">
            <div className="usersHeader">Joined Users</div>
            <div className="usersName">User</div>
            <div className="usersName">User</div>
            <div className="usersName">User</div>
            <div className="usersName">User</div>
            <div className="usersName">User</div>
            <div className="usersName">User</div>
            <div className="usersName">User</div>
            <div className="usersName">User</div>
            <div className="usersName">User</div>
            <div className="usersName">User</div>
            <div className="usersName">User</div>
            <div className="usersName">User</div>
            <div className="usersName">User</div>
            <div className="usersName">User</div>
            <div className="usersName">User</div>
            <div className="usersName">User</div>
            <div className="usersName">User</div>
            <div className="usersName">User</div>
            <div className="usersName">User</div>
        </ScrollToBottom>
    );
};

export default Users;
